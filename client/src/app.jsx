import React from 'react';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Router, Route, Link, browserHistory } from 'react-router'
import routes from './routes.js';
// import { Provider } from 'react-redux';
// import reducer from './reducers/index.jsx';
// import { createStore } from 'redux';


// remove tap delay, essential for MaterialUI to work properly
injectTapEventPlugin();


ReactDom.render((
	<MuiThemeProvider muiTheme={getMuiTheme()}>
		<Router history={browserHistory} routes={routes} />
	</MuiThemeProvider>)
, document.getElementById('react-app'));
