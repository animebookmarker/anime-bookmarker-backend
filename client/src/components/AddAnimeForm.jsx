import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
// {successMessage && <p className="success-message">{successMessage}</p>}

const AddAnimeForm = ({
	onSubmit,
	handleChange,
	errors,
	successMessage,
	anime}) => (
	<Card className="Container">
		<form action="/"  onSubmit={onSubmit}>
	  		{errors.summary && <p className="error-message">{errors.summary}</p>}

      		<div className="field-line">
		        <TextField
		          floatingLabelText="Anime Name"
		          name="anime_name"
		          errorText={errors.anime}
		          onChange={handleChange}
		          value={anime.anime_name}
		        />
			</div>
			<div className="field-line">
		        <TextField
		          floatingLabelText="Following Status"
		          name="following_status"
		          errorText={errors.status}
		          onChange={handleChange}
		          value={anime.following_status}
		        />
			</div>
			<div className="button-line">
				<RaisedButton type="submit" label="Submit" primary />
			</div>
		</form>
	</Card>
);

AddAnimeForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	handleChange: PropTypes.func.isRequired,
	errors: PropTypes.object.isRequired,
	successMessage: PropTypes.string.isRequired,
	anime: PropTypes.object.isRequired
};

export default AddAnimeForm;