import React, { PropTypes } from 'react';
import {AnimeItemActionIcon} from '../containers/AnimeItemActionIcon.jsx';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const AnimeItem = ({id, name}) => (
  <TableRow>
    <TableRowColumn>{id}</TableRowColumn>
    <TableRowColumn>{name}</TableRowColumn>
    <TableRowColumn>{AnimeItemActionIcon}</TableRowColumn>
  </TableRow>
);

AnimeItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default AnimeItem;
