import React, { PropTypes } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import User from '../modules/User.js';
import Auth from '../modules/Auth.js';

/*
/remove/anime
{anime_name:””,username:””}
{errors:{summary:”*”}, successMessage:””}
post


/edit/anime
{anime_name:””,following_status:””,username:””}
{errors:{summary:”*”}, successMessage:””}
post

*/
// <MenuItem primaryText="Edit" onTouchTap={onEditClick(anime.anime_name)} />

const AnimeItemAction = ({anime,openMenu,onRemoveClick}) =>{
	return (
	<IconMenu
		iconButtonElement={<IconButton><MoreVertIcon /></IconButton>} 
		open={openMenu} 
  	>

		<MenuItem primaryText="Remove" onTouchTap={onRemoveClick} /> 
	</IconMenu>
)};

export default AnimeItemAction;