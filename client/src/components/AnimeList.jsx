import React, { PropTypes } from 'react';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import AnimeItem from './AnimeItem.jsx';

const AnimeList = ({animeData}) => (
  <div>
  	{animeData.map((anime)=>(
  		<AnimeItem  id={anime.id} name={anime.name} />
  	))} 
  </div>
);


AnimeList.propTypes = {
  animeData: PropTypes.object.isRequired,
};

export default AnimeList;

