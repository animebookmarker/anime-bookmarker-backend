import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import Auth from '../modules/Auth';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import TopLeftDropMenu from './TopLeftDropMenu.jsx';
import User from '../modules/User';
import Messages from '../modules/Messages';

const styles = {
  title: {
    cursor: 'pointer',
  },
};

function logout(event){
  event.preventDefault();
  const xhr = new XMLHttpRequest();
  

  // create a string for an HTTP body message
  const username = encodeURIComponent(User.getUserInformation().username);

  const formData = `username=${username}`;
  console.log("FUCK YOU SKUAY")
  xhr.open('get', 'http://localhost:8080/api/logout?' + formData);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.responseType = 'json';
  xhr.send();
  Messages.removeMessages();
}

const right = () => {
  if(Auth.isUserAuthenticated()){
    return (<FlatButton onTouchTap={() => {logout()}} label="log out" href="/logout"/>);
  }
};

const tap = ()=>{
  return (window.location = "/");
}


const Base = ({ children }) => ( 
  <div>
    <div>
      <AppBar 
        title={<span style={styles.title}>Animark</span>}
        onTitleTouchTap={tap}
        iconElementRight={right()}
        iconElementLeft={TopLeftDropMenu()}
      />
    </div>
    {children}
  </div>
);

Base.propTypes = {
  children: PropTypes.object.isRequired
};

export default Base;
