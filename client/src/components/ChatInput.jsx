import React, { PropTypes } from 'react';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import TextField from 'material-ui/TextField';


const styles = {
	chatInput: {
		marginTop: 5,
		marginRight: 20,
		marginLeft: 10
	}
}

const ChatInput = ({submitHandler,textChangeHandler,chatInput}) => {
	return (
		<form onSubmit={submitHandler}>
	        <TextField type="text"
	          onChange={textChangeHandler}
	          value={chatInput}
	          hintText="Write a message..."
	          style={styles.chatInput}
	          />
      	</form>
	);
}

export default ChatInput;