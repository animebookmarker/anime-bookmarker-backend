import React, { PropTypes } from 'react';
import ChatroomMessageContainer from '../containers/ChatroomMessageContainer.jsx';
import ChatInputContainer from '../containers/ChatInputContainer.jsx';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import {GridList, GridTile} from 'material-ui/GridList';

const styles = {
	container: {
		display: 'flex',
		flexDirection: 'column'
	}
};

const Chatroom = ({sendHandler,messages}) => {
	return (
		<div className={styles.container}>
			<ChatroomMessageContainer messages={messages} />
			<ChatInputContainer onSend={sendHandler} />
		</div>
	);
};

export default Chatroom;