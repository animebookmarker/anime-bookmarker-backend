import React, { PropTypes } from 'react';
import Message from './Message.jsx';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';


const styles = {
	messages: {
		overflowy: 'scroll',
		overflowx: 'hidden',
		flexGrow: 1,
		padding: 20,
  	}
}

const ChatroomMessage = ({messages}) => {
	const msgs = messages.map((message,i) => {
		return (
          <Message
            key={i}
            // username={message.username}
            message={message.message}
            fromMe={message.fromMe} />
        );
	});
	return (
		<div className={styles.messages} id='messageList'>
			{msgs}
		</div>
	);
};

export default ChatroomMessage;
