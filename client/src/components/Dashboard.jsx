import React, { PropTypes } from 'react';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {Tabs, Tab} from 'material-ui/Tabs';


import AnimeItemActionIcon from '../containers/AnimeItemActionIcon.jsx';
import AddAnimeFormPage from '../containers/AddAnimeFormPage.jsx';
import ChatroomContainer from '../containers/ChatroomContainer.jsx';


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
    overflowY: 'auto',
    
  },
  titleStyle: {
    color: 'rgb(0, 188, 212)',
  },
  addButton:{
    marginLeft: 20,
    marginRight: 20
  }
};

const Dashboard = ({username, animes, animeReloadHandler}) => {
  return (

  <div style={styles.root}>
    <GridList cellHeight='auto' style={styles.gridList} children={2} cols={3}>
      <GridTile cols={2}  titleStyle={styles.titleStyle}>
        <Tabs>
          <Tab label="Your bookmark" onActive={animeReloadHandler}> 
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn>Anime Name</TableHeaderColumn>
                  <TableHeaderColumn>Total Episodes</TableHeaderColumn>
                  <TableHeaderColumn>Status</TableHeaderColumn>
                  <TableHeaderColumn>Action</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody>
                {animes.map((anime)=>(
                  <TableRow >
                    <TableRowColumn>{anime.anime_name}</TableRowColumn>
                    <TableRowColumn>{anime.total_ep}</TableRowColumn>
                    <TableRowColumn>{anime.status}</TableRowColumn>
                    <TableRowColumn>
                      <AnimeItemActionIcon anime={anime} />
                    </TableRowColumn>
                  </TableRow>
                  // <AnimeItem  id={anime.id} name={anime.name} />
                ))} 
              </TableBody>
            </Table>
          </Tab>
          <Tab label="Add anime">
            <AddAnimeFormPage />
          </Tab>
        </Tabs>
      </GridTile>
      <GridTile cols={1}>
        <Tabs>
          <Tab label="Chatroom">
            <ChatroomContainer username={username} />
          </Tab>
        </Tabs>
      </GridTile>
    </GridList>
  </div>
)}

Dashboard.propTypes = {
  username: PropTypes.string.isRequired,
  animes: PropTypes.array.isRequired
};

export default Dashboard;



// class AnimeButton extends React.Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       menuOpen: false
//     }
//   }

//   handleClose() {
//     this.setState({menuOpen: false})
//   }

//   handleOpen() {
//     this.setState({menuOpen: true})
//   }

//   render() {
//     return (
//       <div>
//         <RaisedButton label="click me" onTouchTap={() => this.handleOpen()}/>
//         <Dialog
//           title="Action"
//           modal={false}
//           open={this.state.menuOpen}
//           onRequestClose={() => this.handleClose()}
//         />
//       </div>
//     )
//   }

// }
