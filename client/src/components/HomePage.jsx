import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardTitle, CardActions } from 'material-ui/Card';
import { Link } from 'react-router';

const style = {
  margin: 12,
};

const HomePage = () => (
  <Card className="container">
    <CardTitle title="Animark" subtitle="An Anime Bookmarker and Chatbot." />
    <CardActions>
    	<RaisedButton label="Log in" primary={true} href="/login" style={style}/>
      	<RaisedButton label="Sign up" secondary={true} href="/signup" style={style}/>
    </CardActions>
  </Card>
);

export default HomePage;
