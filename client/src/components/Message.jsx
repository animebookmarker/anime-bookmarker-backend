import React, { PropTypes } from 'react';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';


const styles = {
	message : {
		fromMe: {
			messageBody: {
				backgroundColor: '#af9570',
				color: 'white',
				display: 'inline-block',
				justifyContent: 'flex-end',
				width: '100%',
				paddingLeft: 20,
				paddingTop: 10,

			}
		}
	},
	messageBody: {
		width: '100%',
		display: 'inline-block',
		padding: 20,
		backgroundColor: '#eee',
		border: 1,
		borderRadius: 5,
		paddingRight: 50,
		marginTop: 10,
		marginBottom: 10
	},
}

const Message = ({fromMe,message}) => {
	const fm = fromMe ? 'from-me' : '';
	if(fm){
		return (
			<Card style={styles.message.fromMe.messageBody}>
				<div>
			          	{ message }
		      	</div>
	      	</Card>
		);
	}
	else{
		return (
			<Card style={styles.messageBody}>
				<div>
			          { message }
		      	</div>
		     </Card>
		);
	}
};

export default Message;