import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';


function logout(event){
  event.preventDefault();
  const xhr = new XMLHttpRequest();
  

  // create a string for an HTTP body message
  const username = encodeURIComponent(User.getUserInformation().username);

  const formData = `username=${username}`;
  console.log("FUCK YOU SKUAY")
  xhr.open('get', 'http://localhost:8080/api/logout?' + formData);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.responseType = 'json';
  xhr.send();
  Messages.removeMessages();
}

//The customized component of the verticle menu beside the react app title
//If user is authenticated, show log out to the user when clicked
//otherwise, show if they want to sign up or log in
const TopLeftDropMenu = () => {
  if(!Auth.isUserAuthenticated()){
    return (
    <div>
      <IconMenu
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
        targetOrigin={{horizontal: 'left', vertical: 'top'}}
      >
        <MenuItem primaryText="Log in" href="/login"/>
        <MenuItem primaryText="Sign up" href="/signup"/>
      </IconMenu>
    </div>
    );
  }else{
    return (
    <div>
      <IconMenu
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
        targetOrigin={{horizontal: 'left', vertical: 'top'}}
      >
        <MenuItem primaryText="Log out" href="/logout" onTouchTap={() => logout()}/>
      </IconMenu>
    </div>
    );
  }
};

export default TopLeftDropMenu;
