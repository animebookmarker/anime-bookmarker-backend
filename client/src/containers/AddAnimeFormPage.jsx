import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import User from '../modules/User';
import AddAnimeForm from '../components/AddAnimeForm.jsx';


class AddAnimeFormPage extends React.Component{
	constructor(props, context) {
		super(props,context);

		const storedMessage = localStorage.getItem('successMessage');
		let successMessage = '';

		if (storedMessage) {
			successMessage = storedMessage;
			localStorage.removeItem('successMessage');
		}

		// set the initial component state
		this.state = {
			errors: {},
			successMessage,
			anime: {
				anime_name: '',
				following_status: ''
			}
		};

		this.processForm = this.processForm.bind(this);
		this.changeAnime = this.changeAnime.bind(this);
	}

	changeAnime(event){
		const field = event.target.name;
		const anime = this.state.anime;
		anime[field] = event.target.value;

		this.setState({
			anime
		});
	}

	processForm(event) {
		
		// prevent default action. in this case, action is the form submission event
    	event.preventDefault();

  //   	// create a string for an HTTP body message
		const anime_name = this.state.anime.anime_name;
		const following_status = this.state.anime.following_status;
		const username = encodeURIComponent(User.getUserInformation().username);
		const formData = `username=${username}&anime_name=${anime_name}&following_status=${following_status}`;
		// // create an AJAX request
		const xhr = new XMLHttpRequest();


		//CHANGE THE URL HERE
		xhr.open('post', 'http://localhost:8080/api/add/anime?' + formData);
	
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.responseType = 'json';
		xhr.addEventListener('load', () => {
			if (xhr.status >= 200 && xhr.status <= 299) {
				// success
				// change the component-container state
				this.setState({
				  errors: {},
				  // successMessage: xhr.response.successMessage
				});

				// change the current URL to /
				this.context.router.replace('/');
			} else {
				// failure

				// change the component state
				const errors = xhr.response.errors ? xhr.response.errors : {};
				errors.summary = xhr.response.message;



				this.setState({
					errors
				});
			}
		});
		console.log(anime_name);
		console.log(xhr);
		xhr.send(formData);
		console.log(xhr);
	}

	render() {
		return (
			<AddAnimeForm
				onSubmit={this.processForm}
				handleChange={this.changeAnime}
				errors={this.state.errors}
				successMessage={this.state.successMessage}
				anime={this.state.anime}
			/>
		);
	}
}
AddAnimeFormPage.contextTypes = {
  router: PropTypes.object.isRequired
};


export default AddAnimeFormPage;