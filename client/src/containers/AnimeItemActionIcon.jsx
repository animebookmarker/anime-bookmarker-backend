import React, { PropTypes } from 'react';
import AnimeItemAction from '../components/AnimeItemAction.jsx';
import User from '../modules/User.js';
import Messages from '../modules/Messages.js'

class AnimeItemActionIcon extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {};
        this.onRemoveClick = this.onRemoveClick.bind(this);
	}

    onRemoveClick(event){

        event.preventDefault();

        const xhr = new XMLHttpRequest();
        const username = encodeURIComponent(User.getUserInformation().username);
        const formData = `username=${username}&anime_name=${this.props.anime.anime_name}`;
        xhr.open('post', 'http://localhost:8080/api/remove/anime?' + formData);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.responseType = 'json';
        // xhr.addEventListener('load', () => {
        //   if (xhr.status >= 200 || xhr.status <= 299) {
        //     // change the current URL to /
        //     // this.context.router.replace('/');
        //   }else{
        //     const errors = xhr.response.errors ? xhr.response.errors : {};
        //     errors.summary = xhr.response.message;
        //     this.setState({
        //       errors
        //     });
        //   }
        // });
        xhr.send();
    }

	handleOpenMenu(){
    	this.setState({
    		openMenu: true,
    	});
  	}

	render() {
	    return (
	      <AnimeItemAction anime={this.props.anime} openMenu={this.state.openMenu} onRemoveClick={this.onRemoveClick}/>
    	);
  	}
}
AnimeItemActionIcon.propTypes = {
    anime: PropTypes.object.isRequired
};


export default AnimeItemActionIcon;
