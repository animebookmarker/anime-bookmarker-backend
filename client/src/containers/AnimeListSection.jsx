import React, { PropTypes } from 'react';
import AnimeList from '../components/AnimeList.jsx';
import User from '../modules/User.js';

class AnimeListSection extends React.Component {
	constructor(props, context) {
    	super(props, context);
    	this.state = {
    		animeData: [
    			{id: '1', name: 'hello'}, 
    			{id: '2', name: 'hello'}, 
    			{id: '3', name: 'hello'}
    		]};
	}

	// componentDidMount(){
	// 	const xhr = new XMLHttpRequest();
	//     xhr.open('get', '/api/dashboard');
	//     xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	//     xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
	//     xhr.responseType = 'json';
	//     xhr.addEventListener('load', () => {
	//       const user = User.getUserInformation();
	//       if (xhr.status === 200) {
	//         this.setState({
	//           animeData: xhr.response.animeData,
	//         });
	//       }
	//     });
	//     xhr.send();
	// }

	render(){
		<AnimeList animeData={this.state.animeData}/>
	}
}

export default AnimeListSection;
