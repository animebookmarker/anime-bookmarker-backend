import React, { PropTypes } from 'react';
import ChatInput from '../components/ChatInput.jsx';

class ChatInputContainer extends React.Component{
	constructor(props){
		super(props);
		this.state = { chatInput: '' };

		this.submitHandler = this.submitHandler.bind(this);
    	this.textChangeHandler = this.textChangeHandler.bind(this);
	}
	textChangeHandler(event)  {
		// event.preventDefault();
		this.setState({ chatInput: event.target.value });
	}
	submitHandler(event) {
		// Stop the form from refreshing the page on submit
		event.preventDefault();
		
		// Call the onSend callback with the chatInput message
		// console.log(this.state.chatInput)
		this.props.onSend(this.state.chatInput);

		// Clear the input box
		this.setState({ chatInput: '' });
	}

	render(){
		return (
			<ChatInput 
				submitHandler={this.submitHandler}
				textChangeHandler={this.textChangeHandler}
				chatInput={this.state.chatInput}
			/>
		);
	}
}	
export default ChatInputContainer;