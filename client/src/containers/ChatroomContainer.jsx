import React, { PropTypes } from 'react';
import Chatroom from '../components/Chatroom.jsx';
import Messages from '../modules/Messages';
class ChatroomContainer extends React.Component{
	constructor(props){
		super(props);
		this.state = {messages: []}
		// this.sendHandler = this.sendHandler.bind(this);
		this.sendHandler = this.sendHandler.bind(this);
	}

	createAnimeInformationString(anime){
		// var str = 'Anime Name: ' + anime.anime_name + '\r\n';
		// str += 'Total Number of episodes: '  + anime.total_ep + '\r\n';
		// str += 'Starting Date: '  + anime.start_date + '\r\n';
		// str += 'Ending Date: '  + anime.end_date + '\r\n';
		// str += 'Status: '  + anime.status + '\r\n';
		// str += 'Score: '  + anime.score + '\r\n';
		// str += 'Url to the image: '  + anime.image_url + '\r\n';

		var str = anime.anime_name + " has " + anime.total_ep + " episode. It started airing on "
				+ anime.start_date + " and ended on " + anime.end_date + " and has an average " +
				"score of " + anime.score;
		return str
	}

	sendHandler(message){
		// event.preventDefault();
		/*/information/anime {user_request: user input in the chat window}*/
		const messageObject = {
			username: this.props.username,
			message
		};

		const xhr = new XMLHttpRequest();
    
		const formData = `user_request=${messageObject.message}`;
		xhr.open('get', 'http://localhost:8080/api/information/anime?' + formData);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.responseType = 'json';
		xhr.addEventListener('load', () => {
			if (xhr.status >= 200 && xhr.status <= 299) {
				const errors = xhr.response.errors ? xhr.response.errors : {};

				if(xhr.response.anime){
					const constructed = this.createAnimeInformationString(xhr.response.anime);
					const reply = {
						message: constructed
					};
					this.addMessage(reply);
				}else{
					const reply = {
						message: xhr.response.errors.summary
					};
					this.addMessage(reply);
				}
				
				// Messages.addMessage(constructed);
			}else{
				const reply = {
					message: xhr.response.errors.summary
				};
				this.addMessage(reply);
				
		        this.setState({
		          errors
		        });
			}
		});

		
		xhr.send();
		messageObject.fromMe = true;
 		this.addMessage(messageObject);
 		// Messages.addMessage(messageObject);
	}

	// sendHandler(message){
	// 	const messageObject = {
	// 		username: this.props.username,
	// 		message
	// 	};

	// 	//this.socket.emit(‘client:message’, messageObject);s

	// 	const xhr = new XMLHttpRequest();
    
	// 	const username = encodeURIComponent(User.getUserInformation().username);
	// 	const formData = `username=${messageObject.username}&msg=${messageObject.message}`;


	// 	xhr.open('get', 'http://localhost:8080/api/talk?' + formData);
	// 	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	// 	xhr.responseType = 'json';
	// 	xhr.addEventListener('load', () => {
	// 		if (xhr.status >= 200 || xhr.status <= 299) {
	// 			const errors = xhr.response.errors ? xhr.response.errors : {};
	// 			const msg = this.state.messages; 
	// 			msg.push(xhr.response.msg);
	// 			this.setState({
	// 				messages: msg
	// 			})
	// 		}else{
	// 			const errors = {};
	// 	        errors.summary = "Server is unavailable at the moment";
	// 	        this.setState({
	// 	          errors
	// 	        });
	// 		}
	// 	});

	// 	xhr.send();
	// 	messageObject.fromMe = true;
 // 		this.addMessage(messageObject);
	// }

	addMessage(message) {
    	// Append the message to the component state
		const messages = this.state.messages;
		messages.push(message);
		this.setState({ messages });
	}

	render(){
		console.log(this.state.messages)
		return <Chatroom messages={this.state.messages} sendHandler={this.sendHandler} />
	}
}

export default ChatroomContainer;