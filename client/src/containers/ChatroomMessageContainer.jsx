import React, { PropTypes } from 'react';
import ChatroomMessage from '../components/ChatroomMessage.jsx';

class ChatroomMessageContainer extends React.Component{
	constructor(props){
		super(props);

	}
	componentDidUpdate() {
    	// get the messagelist container and set the scrollTop to the height of the container
		const objDiv = document.getElementById('messageList');
		objDiv.scrollTop = objDiv.scrollHeight;
	}

	render(){
		return <ChatroomMessage messages={this.props.messages} />
	}
}

export default ChatroomMessageContainer;