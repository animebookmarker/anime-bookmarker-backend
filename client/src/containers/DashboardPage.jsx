import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import User from '../modules/User';
import Dashboard from '../components/Dashboard.jsx';
import AnimeListSection from './AnimeListSection.jsx';


class DashboardPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props,context) {
    super(props,context);

    this.state = {
      user: {username : ''},
      animes: []
    };

    this.getAnimeList = this.getAnimeList.bind(this);
  }


  getAnimeList(){
    const xhr = new XMLHttpRequest();
    
    const username = encodeURIComponent(User.getUserInformation().username);
    const formData = `username=${username}`;
    xhr.open('get', 'http://localhost:8080/api/view?' + formData);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.addEventListener('load', () => {
      if (xhr.status >= 200 && xhr.status <= 299) {
        const errors = xhr.response.errors ? xhr.response.errors : {};
        if(xhr.response.successMessage){
          const animes = xhr.response.animes;
          this.setState({
            animes
          });
        }
        this.setState({
          errors
        });

      }else{
        const errors = {};
        errors.summary = "Server is unavailable at the moment";
        this.setState({
          errors
        });
      }
    });
    xhr.send();
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
    const xhr = new XMLHttpRequest();
    const username = encodeURIComponent(User.getUserInformation().username);
    const token = Auth.getToken();

    xhr.open('get', `http://localhost:8080/api/dashboard?username=${username}&token=${token}`);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';

    xhr.addEventListener('load', () => {
      if (xhr.status >= 200 || xhr.status <= 299) {
        // success --> server is up

        // console.log(xhr.response)
        // change the component-container state
        const errors = xhr.response.errors ? xhr.response.errors : {};
        if(xhr.response.successMessage){
          // localStorage.setItem('successMessage', xhr.response.successMessage);
          this.getAnimeList();
        }
        else{
          Auth.deauthenticateUser();
          User.removeUserInformation();
        }
        
        this.setState({
          errors
        });

        // make a redirect
        this.context.router.replace('/');
      } 
      else {
        // failure
        const errors = {};
        errors.summary = "Server is unavailable at the moment";
        this.setState({
          errors
        });
      }
    });

    xhr.send();
  }

  /**
   * Render the component.
   */
  render() {
    return (<Dashboard 
      username={this.state.user.username}
      animes={this.state.animes}
      animeReloadHandler = {this.getAnimeList}
    />);
  }
}

DashboardPage.contextTypes = {
  router: PropTypes.object.isRequired
};

export default DashboardPage;
