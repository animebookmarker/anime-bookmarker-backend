import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import User from '../modules/User';
import LoginForm from '../components/LoginForm.jsx';
import Messages from '../modules/Messages';

class LoginPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    // set the initial component state
    this.state = {
      errors: {},
      successMessage,
      user: {
        username: '',
        email: '',
        password: ''
      }
    };

    this.processForm = this.processForm.bind(this);
    this.changeUser = this.changeUser.bind(this);
  }

  /**
   * Process the form.
   *
   * @param {object} event - the JavaScript event object
   */
  processForm(event) {
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();

    // create a string for an HTTP body message
    const username = encodeURIComponent(this.state.user.username);
    // const email = encodeURIComponent(this.state.user.email);
    const password = encodeURIComponent(this.state.user.password);
    const formData = `username=${username}&password=${password}`;

    // create an AJAX request
    var xhr = new XMLHttpRequest();


    //CHANGE THE URL HERE
    xhr.open('post', 'http://localhost:8080/api/login');

    // xhr.setRequestHeader('Access-Control-Allow-Origin','true');
    
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.addEventListener('load', () => {
      // console.log(xhr.status)
      if (xhr.status >= 200 || xhr.status <= 299) {
        // success --> server is up

        // console.log(xhr.response)
        // change the component-container state
        const errors = xhr.response.errors ? xhr.response.errors : {};
        if(xhr.response.successMessage){
          // localStorage.setItem('successMessage', xhr.response.successMessage);
          Auth.authenticateUser(xhr.response.token);
          User.setUserInformation({username: this.state.user.username});
          Messages.setMessages();
        }
        
        this.setState({
          errors
        });
        // make a redirect
        this.context.router.replace('/');
      } 
      else {
        // failure
        const errors = {};
        errors.summary = "Server is unavailable at the moment";
        this.setState({
          errors
        });
      }
    });
    xhr.send(formData);
  }

  /**
   * Change the user object.
   *
   * @param {object} event - the JavaScript event object
   */
  changeUser(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  /**
   * Render the component.
   */
  render() {
    return (
      <LoginForm
        onSubmit={this.processForm}
        onChange={this.changeUser}
        errors={this.state.errors}
        successMessage={this.state.successMessage}
        user={this.state.user}
      />
    );
  }
}


// const mapStateToProps = (state) => {
//   return {
//     user: state.user_session.user
//   };
// }

// function mapDispatchToProps(dispatch) {
//   return { 
//       actions: bindActionCreators(userSessionCreators, dispatch)
//     };
// } 

LoginPage.contextTypes = {
  router: PropTypes.object.isRequired
};


export default LoginPage;
