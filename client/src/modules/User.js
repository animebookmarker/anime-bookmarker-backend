class User {
  static setUserInformation(user){
  	localStorage.setItem("user_session", JSON.stringify(user));
    // localStorage.setItem('user_session',user);
  }

  static getUserInformation(){
  	var user = localStorage.getItem("user_session");
  	user = JSON.parse(user);
    return user;

  }

  static removeUserInformation(){
    return localStorage.removeItem();
  }
}

export default User;
