const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const config = require('./config');

const app = express();


// tell the app to look for static files in these directories
app.use(express.static('./client/src/static'));
app.use(express.static('./client/dist/'));
// tell the app to parse HTTP body messages
app.use(bodyParser.urlencoded({ extended: false }));


//Put this as the last line to redirect evreything that is not node/server related to react first
//all above lines are route/server/node related so we have to laid down the route for that
app.get("/*",function(req, res) {res.sendFile(__dirname + '/client/src/static/index.html')})

// start the server
app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000 or http://127.0.0.1:3000');
});
